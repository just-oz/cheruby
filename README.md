# Cheruby

## Description
Cheruby covers the "switch ruby version" use case by using chroot.
With Cheruby you can keep your developement environment clean and uncluttered, and
still run ruby programs, typically "rake test" with multiple ruby versions .


## Useage
```
$ bin/cheruby 
Commands:
  cheruby bind_hdev          # bind ~/dev directory into current berry
  cheruby bind_ruby version  # bind ruby version install directory into the chroot
  cheruby call CMD           # run CMD synchronously with popen3
  cheruby create_user        # create user and his group in the current berry
  cheruby exec CMD           # run CMD with exec 
  cheruby help [COMMAND]     # Describe available commands or one specific command
  cheruby last_used          # Show last used berry and ruby version
  cheruby login              # login as user
  cheruby mount3             # mount proc & sys & dev if needed
  cheruby mount4             # mount /proc & /sys & /dev & ~/dev if needed
  cheruby set_berry bvers    # Set the berry version to be used in next command(s)
  cheruby unbind_ruby        # unbind ruby install directory from the chroot
  cheruby version            # get currently active ruby version
```
