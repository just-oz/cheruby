# frozen_string_literal: true

module Cheruby
  # Design: the Invalid module and the Valid class share exactly the same
  # methods (ie. interface) so methods returning such objects can be
  # post-processed in a declarative way
  # Example:
  #   something.do_stuff(param).tap_valid{|valid_result| ... }
  #                             .tap_error{|error|  ... }

  module Contract
    # represents a invalid result, ie. an error
    # this shall be included/extended to our Error classes thus
    # automagically making them contract/flow enabled

    # All tap_valid* handlers are not executed
    #     tap_error* handlers are executed
    module Invalid
      def tap_error
        yield self
        self # allow chaining
      end

      def tap_valid
        self # allow chaining
      end

      def if_valid
        self
      end

      def if_error
        yield self ## return this
      end

      def if_valid_collect
        self
      end

      def map_result!
        self # allow chaining
      end

      def collect_result!
        self # allow chaining
      end

      def error
        self
      end

      def result
        nil
      end
    end

    class Error
      include Invalid
    end

    # represents a valid result.
    # All tap_valid* handlers are executed
    #     tap_error* handlers are not executed
    class Valid
      attr_reader :result

      def initialize(result)
        @result = result
      end

      def tap_error
        self # allow chaining
      end

      def tap_valid
        yield @result
        self # allow chaining
      end

      def if_valid
        yield @result ## return this
      end

      def if_error
        self # allow chaining
      end

      def if_valid_collect
        yield(*@result) ## return this
      end

      def map_result!
        @result = yield @result
        self # allow chaining
      end

      def collect_result!
        @result = yield(*@result)
        self # allow chaining
      end

      def error
        nil
      end
    end # class Valid

    def self.valid(result)
      Contract::Valid.new(result)
    end

    def self.and(*contracts)
      # pick the first error if any
      if (ff = contracts.find(&:error))
        return ff
      end

      # return a new one with @result = list of the contracts's results
      # usually this then be reduced again with #collect_result! or #               #if_valid_collect methods
      valid(contracts.map(&:result))
    end

    # shortcut for Contract.and(*contracts).collect_result!
    def self.collect_result!(*contracts)
      # pick the first error if any
      if (ff = contracts.find(&:error))
        return ff
      end

      # return a new one with @result = yield(*list of the contracts's results)
      valid(yield(*contracts.map(&:result)))
    end

    # generic success return, when return value does not matter
    # but usefull for control-flow
    OK = Contract.valid(nil).freeze
    # generic error return, when error value does not matter
    # but usefull for control-flow
    NOT_OK = Error.new.freeze
    NOK = NOT_OK # it's shorter
  end # Contract
end

