
require 'thor'
require 'open3'
require 'yaml'
require 'pathname'

require_relative 'contract'

$conf = YAML.safe_load(File.read('conf/cheruby.yaml'))

module Cheruby

  class Error < StandardError
    include Cheruby::Contract::Invalid 
  end
  
  class Command
  

    class FailedResult  < Cheruby::Contract::Error
      def initialize(res)
        @result = res
      end
      def output_and_exit
        @result.output_and_exit
      end
      
    end   
    class Result
      attr_reader :stdout 
      attr_reader :stderr 
      attr_reader :status 
      def initialize(sout, serr, sta)
        @stdout = sout
        @stderr = serr
        @status = sta
      end
      
      def contract
        @status.success? ? Contract.valid(self) : FailedResult.new(self)
      end
      
      def output
        STDOUT.puts @stdout
        STDERR.puts @stderr
      end
      
      def output_and_exit
        STDOUT.puts @stdout
        STDERR.puts @stderr
        exit @status.success?
      end
      
    end
    
    def initialize(istr)
      @invoke_string = istr
    end
       
    def call
      Result.new(*::Open3.capture3(@invoke_string))
    end
    
    def exec
      ::Kernel.exec @invoke_string
    end
    
    def call_and_exit
      call.output_and_exit
    end
    
  end
  class Sudo < Command
    def initialize(istr)
      @invoke_string = "sudo #{istr}"
    end
  end
  class Chroot
    attr_accessor :user
    attr_accessor :group
    attr_reader :user_home
    
    def self._last_used
      last = YAML.safe_load(File.read('var/last_used.yaml'))
      self.from_berry(last['berry'])
    end
    def self.last_used
      self._last_used.tap_error{|e| puts e.to_s ; Kernel.exit false }
                     .tap_valid { |chrb| yield chrb }
    end
    def self._check_bervers(bervers)
      return Error.new("Unregistered berry version. Please check config/setup") unless b = $conf['berries']['versions'][bervers]
      Contract.valid(bervers)
    end
    def self.check_bervers(bervers)
      self._check_bervers(bervers).tap_error{|e| puts e.to_s ; Kernel.exit false }
                     .tap_valid { |bv| yield bv }
    end
    def self.set_berry(bervers)
    
      last = {'berry' => bervers }
      File.open('var/last_used.yaml', 'w') do |f|
        f.write(YAML.dump(last))
      end
      
    end
    def self.for_ruby_version(vers)
      return Error.new("Unregistered ruby version. Please check config/setup") unless v = $conf['rubies']['versions'][vers]
      return Error.new("Unregistered berry version for this ruby version. Please check config/setup") unless berry = v['berry']
      self.from_berry(berry)
    end
    
    def self.from_berry(bervers)
      self._check_bervers(bervers).map_result! do |bervers|
      
        self.set_berry(bervers)
      
        chrb = self.new(bvers: bervers)
        chrb.output_info                  
        chrb
      end
    end
    
    def initialize(bvers:)
      @berries = $conf['berries']
      @user = $conf['user']['name']
      @group = $conf['group']['name']      
      @base_path = Pathname.new(@berries['base_path'])
      @bvers = bvers
      bversion = @berries['versions'][@bvers]
      @path = @base_path + bversion['relpath']    
    

      @rubies = Pathname.new($conf['rubies']['base_path'])
      @user_home = Pathname.new("/home/#{@user}")
    end
    
    def output_info
      rbv_s = nil
      ruby_version.tap_valid{| rbcmd| rbv_s = rbcmd.call.stdout }
                  .tap_error{ |_e| rbv_s =  'none' }
      puts "Using berry: #{@bvers} with ruby version #{rbv_s }"      
    end
    
    def wrap_login(root: nil, dir: nil)
      cmd = %Q</bin/bash  --noprofile --norc -l>
      root ? asroot(cmd, dir: dir) : asuser(cmd, dir: dir )
    end    
    
    def wrap_cmd(cmd, root: nil, dir: nil)
      root ? asroot(cmd, dir: dir) : asuser(cmd, dir: dir )
    end
    
    # changdir lambda
    # CDWRAP = lambda{| cmd, dir| %Q|ruby -e "Dir.chdir(%q<#{dir}>) ; Kernel.exec(%q<#{cmd} >) "| }
    CDWRAP = lambda{| cmd, dir| %Q| bash -c "( cd #{dir} && #{cmd} )" | }
    
    #Subshell lambda
    SHWRAP = lambda{| cmd | %Q| bash -c "( #{cmd} )" | }
    def asroot(cmd, dir: nil)
      cmd = dir ? CDWRAP.call(cmd, dir) : SHWRAP.call(cmd)
      Sudo.new("chroot #{@path} #{cmd}")
    end
    
    def asuser(cmd, dir: nil)
      cmd = dir ? CDWRAP.call(cmd, dir) : SHWRAP.call(cmd)
    
      Sudo.new("chroot --userspec=#{@user}:#{@group} #{@path} #{cmd}")
    end
    # warning here we assume that the only ruby that can exist in
    # the chroot is our self managed one
    # we shall be consistent and dont install ruby from debian package   
    def check_ruby
      asuser('which ruby').call.contract 
    end 
    
    def ruby_version
      check_ruby.map_result!{|_ok| asuser(%q(ruby -e 'puts RUBY_VERSION')) }
    end
    
    def mount_proc
      Sudo.new("mount -t proc /proc #{@path}/proc/").call unless File.exists?("#{@path}/proc/version")
    end
    
    def mount_sys
      Sudo.new("mount --rbind /sys #{@path}/sys").call unless File.exists?("#{@path}/sys/kernel")
    end
 
    def mount_dev
      Sudo.new("mount --rbind /dev #{@path}/dev").call unless File.exists?("#{@path}/dev/disk")
    end    
    
    def mount3
      mount_proc
      mount_sys
      mount_dev
    end
    
    def bind_hdev
      target = Pathname.new "#{@path}/#{user_home}/dev"
      target.mkpath unless target.exist?
      Sudo.new("mount -o bind #{user_home}/dev #{target} " )
    end
    
    def unbind_hdev
      Sudo.new("umount #{@path}/#{user_home}/dev" )
    end    
    
    def mount4
      mount3
      bind_hdev.call_and_exit
    end
    
    def bind_ruby(version)

      Sudo.new("mount -o bind #{@rubies}/#{version}/local #{@path}/usr/local " )
    end
    
    def unbind_ruby
      Sudo.new("umount #{@path}/usr/local" )
    end 
    
    def addgroup
      # addgroup [Optionen] [--gid ID] Gruppe
      grp = $conf['group']
      gid = grp['id']
      gname = grp['name']
      asroot("addgroup --gid #{gid} #{gname}" )
    end

    def adduser
      # adduser    [Optionen]    [--home    VERZEICHNIS]    [--shell     SHELL]
      # [--no-create-home] [--uid ID] [--firstuid ID] [--lastuid ID] [--ingroup
      # GRUPPE | --gid ID]  [--disabled-password]  [--disabled-login]  [--gecos
      # GECOS] [--add_extra_groups] Benutzer

      usr = $conf['user']
      uid = usr['id']
      grp = $conf['group']
      gid = grp['id']
      uname = usr['name']
      asroot("adduser --uid #{uid} --gid #{gid} #{uname}" )
    end
    
  end
  
  class CLI < Thor
    def initialize(*)
      super
    end
    

    desc 'call CMD', 'run CMD synchronously with popen3'
    option :dir, :aliases => [:d], :desc => "chdir to DIR before executing CMD"
    option :root, :aliases => [:r], :type => :boolean, :desc => "execute as root "    
    def call(cmd)
      dir = options[:dir]
      root = options[:root]
      
      Chroot.last_used {|lu| lu.wrap_cmd(cmd, dir: dir, root: root).call_and_exit }    
    end
    
    desc 'exec CMD', 'run CMD with exec  '
    option :dir, :aliases => [:d], :desc => "chdir to DIR before executing CMD"
    option :root, :aliases => [:r], :type => :boolean, :desc => "execute as root "
    def exec(cmd)
      dir = options[:dir]
      root = options[:root]
      
      Chroot.last_used {|lu| lu.wrap_cmd(cmd, dir: dir, root: root).exec }
    end     
       
    desc 'version', 'get currently active ruby version'
    def version
      Chroot.last_used do |lu| 
        lu.ruby_version.tap_valid{| rbcmd| rbcmd.call_and_exit }
                       .tap_error{ |e| e.output_and_exit } 
                         
      end
      
    end
    
    desc 'set_berry bvers', 'Set the berry version to be used in next command(s)'
    def set_berry(bervers)
      Chroot.check_bervers(bervers){|bv| 
        Chroot.set_berry(bv)
        Chroot.new(bvers: bv).output_info
      }
    end    
    
    desc 'bind_ruby version', 'bind ruby version install directory into the chroot'
    def bind_ruby(rbvers)
      Chroot.for_ruby_version(rbvers).tap_valid do |chrb|
        res = chrb.bind_ruby(rbvers).call
        chrb.output_info
        res.output_and_exit      
      end.tap_error{|e| puts e.to_s }
    end
    

    
    desc 'unbind_ruby', 'unbind ruby install directory from the chroot'
    def unbind_ruby
      Chroot.last_used do |lu| 
        res = lu.unbind_ruby.call
        lu.output_info
        res.output_and_exit      
      end
    end    
    
    desc 'login', 'login as user'
    option :dir, :aliases => [:d], :desc => "set the login DIR"
    option :root, :aliases => [:r], :type => :boolean, :desc => "login as root "
    def login
      dir = options[:dir]
      root = options[:root]    
      Chroot.last_used{|lu| 
        #lu.asuser(%Q</bin/bash  --noprofile --norc -l>).exec 
        lu.wrap_login(dir: dir, root: root).exec
      }
    end
    
    desc 'bind_hdev', 'bind ~/dev directory into current berry'
    def bind_hdev
      Chroot.last_used{|lu| lu.bind_hdev.call_and_exit}
    end
    
    desc 'mount3', 'mount proc & sys & dev if needed'
    def mount3
      Chroot.last_used{|lu| lu.mount3 }
    end
    
    desc 'create_user', 'create user and his group in the current berry'
    def create_user
      # first create the group
      Chroot.last_used{|lu|
        lu.addgroup.call.output 
        lu.adduser.exec
      }
    end
    
    desc 'mount4', 'mount /proc & /sys & /dev & ~/dev if needed'
    def mount4
      Chroot.last_used{|lu| lu.mount4 }
    end    
    
    desc 'last_used', 'Show last used berry and ruby version'
    def last_used
      Chroot.last_used{|lu| true }
    end      
  end
  
end

